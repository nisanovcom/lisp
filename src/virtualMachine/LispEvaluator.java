package virtualMachine;

import java.util.HashMap;
import java.util.Map;

import library.Procedure;
import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispProgram;
import lispDataTypes.LispString;
import lispDataTypes.lispNumbers.LispInt;
import virtualMachine.lispProcedures.LispProcedure;
import virtualMachine.lispProcedures.LispUserProcedure;
import virtualMachine.primitives.LispPrimitiveProcedure;

public class LispEvaluator {

    public static LispObject evalSerial(LispProgram program, LispVarTable env) {
        LispObject lastObj = null;
        for (int i = 0; i < program.getProgramSize(); i++) {
            LispObject cmd = program.getCmd(i);
            // System.out.println("excecuting cmd: " + cmd.getStringVal());
            lastObj = eval(cmd, env);
        }
        // System.out.println("evalSerial Returning " + lastObj.getStringVal() + " of
        // type: " + lastObj.getClass().getSimpleName());
        return lastObj;
    }


    public static LispObject eval(LispObject lispObj, LispVarTable env) {
        String cmdType = lispObj.getClass().getSimpleName();

        /*
         * NOTE: the check order has importance!
         */

        // System.out.println("got obj: " + lispObj.getStringVal() + " of type " +
        // cmdType);
        if (isString(lispObj)) {
            // System.out.println("obj is string");
            return lispObj;
        } else if (isInt(lispObj)) {
            // System.out.println("obj is int");
            return lispObj;
        } else if (isCommand(lispObj)) {
            // System.out.println("obj is cmd");
            return evalCommand((LispCommand) lispObj, env);
        } else if (isPrimitive(lispObj)) {
            // System.out.println("obj is primitive");
            return getPrimitiveProc(lispObj);
        } else if (isVarName(lispObj)) {
            // System.out.println("obj " + lispObj.getStringVal() + " is var name");
            return eval(env.getValue(lispObj.getStringVal()), env);
        } else if (isLambda(lispObj)) {
            // System.out.println("obj is lambda");
            return lispObj;
        }

        System.out.println("Error: Unknown lispType at eval at LispEvaluator: " + lispObj.getStringVal());
        System.out.println(isPrimitive(lispObj));
        return null;

        // Map<Procedure<Boolean, LispObject>, Procedure<LispObject, LispObject>>
        // evaluetorMap = new HashMap<>();
        // evaluetorMap.put((obj) -> isString(obj), (obj) -> obj);
        // evaluetorMap.put((obj) -> isInt(obj), (obj) -> obj);
        // evaluetorMap.put((obj) -> isCommand(obj), (obj) -> evalCommand((LispCommand)
        // lispObj, env));
        // evaluetorMap.put((obj) -> isPrimitive(obj), (obj) -> getPrimitiveProc(obj));
        // evaluetorMap.put((obj) -> isLambda(obj), (obj) -> obj);
        // evaluetorMap.put((obj) -> isVarName(obj), (obj) ->
        // eval(env.getValue(obj.getStringVal()), env));
        //
        // for (Procedure<Boolean, LispObject> check : evaluetorMap.keySet()) {
        // if (check.run(lispObj)) {
        // return evaluetorMap.get(check).run(lispObj);
        // }
        // }
        //
        // System.out.println("Error: Unknown lispType at eval at LispEvaluator: " +
        // lispObj.getStringVal());
        // System.out.println(isPrimitive(lispObj));
        // return null;

    }


    private static boolean isLambda(LispObject lispObj) {
        if (lispObj instanceof LispUserProcedure) {
            return true;
        }
        return false;
    }


    private static LispPrimitiveProcedure getPrimitiveProc(LispObject lispObj) {
        return LispPrimitiveProcedure.getPrimitiveProc(lispObj);
    }


    private static LispObject evalCommand(LispCommand lispCmd, LispVarTable env) {
        // System.out.println("Eval cmd: "+ lispCmd.getStringVal());
        LispObject proc = lispCmd.getArg(0);
        LispProcedure cmdProc = (LispProcedure) eval(proc, env);
        // System.out.println("Got procedure type: " +
        // cmdProc.getClass().getSimpleName());
        return cmdProc.evalProcedure(lispCmd, env);
    }


    public static boolean isCommand(LispObject lispObj) {
        if (lispObj instanceof LispCommand) {
            return true;
        }
        return false;
    }


    public static boolean isPrimitive(LispObject lispObj) {
        if (!(lispObj instanceof LispString)) {
            if (lispObj instanceof LispPrimitiveProcedure) {
                return true;
            }
            return false;
        }
        String objVal = ((LispString) lispObj).getValue();
        return LispPrimitiveProcedure.isPrimitiveExists(objVal);
    }


    public static boolean isVarName(LispObject lispObj) {
        if (!(lispObj instanceof LispString)) {
            return false;
        }
        String objVal = ((LispString) lispObj).getValue();
        if (objVal.charAt(0) == '\'') {
            return false;
        }
        return true;
    }


    public static boolean isString(LispObject lispObj) {
        if (!(lispObj instanceof LispString)) {
            return false;
        }
        LispString objStr = (LispString) lispObj;
        String objVal = objStr.getValue();
        char firstChar = objVal.charAt(0);
        char lastChar = objVal.charAt(objVal.length() - 1);
        if (firstChar == '\'' && lastChar == '\'') {
            return true;
        }
        return false;
    }


    public static boolean isInt(LispObject lispObj) {
        if (lispObj instanceof LispInt) {
            return true;
        }
        return false;
    }

}
