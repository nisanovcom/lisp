package virtualMachine.primitives.strPrims;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.lispNumbers.LispInt;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;
import virtualMachine.primitives.LispPrimitiveProcedure;

public class LispStrLen extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        if (lispCmd.getNumArgs() != 2) {
            // TODO add exception here!
            System.out.println("strlen recieves exactly 1 args!");
            int x = 0 / 0;
        }
        LispObject lispString = LispEvaluator.eval(lispCmd.getArg(1), env);
        return new LispInt(lispString.getStringVal().length());
    }

}
