package virtualMachine.primitives.strPrims;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispString;
import lispDataTypes.lispNumbers.LispInt;
import lispDataTypes.lispNumbers.LispNumber;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;
import virtualMachine.primitives.LispPrimitiveProcedure;

public class LispSubstr extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        if (lispCmd.getNumArgs() != 4) {
            // TODO add exception here!
            System.out.println("substr recieves exactly 3 args!");
            int x=0/0;
        }
        LispObject lispString = LispEvaluator.eval(lispCmd.getArg(1), env);
        LispObject startPos = LispEvaluator.eval(lispCmd.getArg(2), env);
        if (!(startPos instanceof LispInt)) {
            // TODO throw exception here
            System.out.println("substr start position accepts only a integer!");
            int x = 0 / 0;
        }

        LispObject endPos = LispEvaluator.eval(lispCmd.getArg(3), env);
        if (!(endPos instanceof LispInt)) {
            // TODO throw exception here
            System.out.println("substr end position accepts only a integer!");
            int x = 0 / 0;
        }
        return new LispString(lispString.getStringVal().substring(((LispInt) startPos).getNumberValue(),
                ((LispInt) endPos).getNumberValue()));
    }

}
