package virtualMachine.primitives;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.lispNumbers.LispInt;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;

public class LispPrint extends LispPrimitiveProcedure {

    // @Override
    // public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
    // LispObject printObj = lispCmd.getArg(1);
    //// System.out.println("PRINTING OBJ: " + printObj.getStringVal());
    // LispObject evaledExpr = LispEvaluator.eval(printObj, env);
    // String objStr = evaledExpr.getStringVal();
    // System.out.println("LISP PRINT >> " + objStr);
    // return new LispInt(1);
    // }

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        String outString = "";
//        outString = "LISP PRINT >> ";
        for(int i=1; i<lispCmd.getNumArgs(); i++) {
            LispObject printObj = lispCmd.getArg(i);
//             System.out.println("PRINTING OBJ: " + printObj.getStringVal());
            LispObject evaledExpr = LispEvaluator.eval(printObj, env);
            String objStr = evaledExpr.getStringVal();
            outString += objStr;
        }
        System.out.println(outString);
        return new LispInt(1);
    }


//    public String getStringVal() {
//        return "LispPrint Primitive";
//    }

}
