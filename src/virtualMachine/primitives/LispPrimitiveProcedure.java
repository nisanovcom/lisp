package virtualMachine.primitives;

import lispDataTypes.LispObject;
import lispDataTypes.LispVarPair;
import virtualMachine.LispVarTable;
import virtualMachine.lispProcedures.LispProcedure;
import virtualMachine.primitives.conditions.LispIf;
import virtualMachine.primitives.conditions.LispIsBigger;
import virtualMachine.primitives.conditions.LispIsEqual;
import virtualMachine.primitives.strPrims.LispStrLen;
import virtualMachine.primitives.strPrims.LispSubstr;

public abstract class LispPrimitiveProcedure extends LispProcedure {
    private static LispVarTable primitivesVarTable = getPrimitivesTable();


    private static LispVarTable getPrimitivesTable() {
        LispVarTable primVT = new LispVarTable();
        // add here all our primitives
        // to allow hebraw codes:
        primVT.addVar(getPrimPair("יבא", new LispImport()));

        primVT.addVar(getPrimPair("define", new LispDefine()));
        primVT.addVar(getPrimPair("set", new LispSet()));

        primVT.addVar(getPrimPair("lambda", new LispLambda()));
        primVT.addVar(getPrimPair("import", new LispImport()));

        primVT.addVar(getPrimPair("+", new LispAdd()));
        primVT.addVar(getPrimPair("-", new LispNegate()));

        primVT.addVar(getPrimPair("if", new LispIf()));
        primVT.addVar(getPrimPair(">", new LispIsBigger()));
        primVT.addVar(getPrimPair("=?", new LispIsEqual()));

        primVT.addVar(getPrimPair("print", new LispPrint()));

        primVT.addVar(getPrimPair("substr", new LispSubstr()));
        primVT.addVar(getPrimPair("strlen", new LispStrLen()));
        // System.out.println("-------------------------");
        // ---------------------------

        return primVT;
    }


    private static LispVarPair getPrimPair(String primName, LispPrimitiveProcedure proc) {
        return new LispVarPair(primName, proc);
    }


    public static LispPrimitiveProcedure getPrimitiveProc(LispObject lispObj) {
        if (lispObj instanceof LispPrimitiveProcedure) {
            return (LispPrimitiveProcedure) lispObj;
        }
        String primName = lispObj.getStringVal();
        return (LispPrimitiveProcedure) primitivesVarTable.getValue(primName);
    }


    public static boolean isPrimitiveExists(String name) {
        return primitivesVarTable.isVarExists(name);
    }


    public String getStringVal() {
        return "Lisp_Primitive_" + this.getClass().getSimpleName();
    }


    public static String dumpCommands() {
        String cmds = "";
        cmds += "import -- imports the compiles and runs another file and thus inserts its var table to current var table\n";

        cmds += "define -- defines a var in current scope even if exists in outer scopes\n";
        cmds += "set -- changes the value of existing var in scope, will cause crash if var doesnt exist\n";

        cmds += "lambda -- creates a lambda in the form: lambda (arg1 arg2 etc) (body)\n";

        cmds += "+ -- adds 2 numbers\n";
        cmds += "- -- subtracts 2 numbers\n";

        cmds += "if -- an if condition in the form: if bool (if-true-body) (if-false-body)\n";
        cmds += "> -- returns if first arg bigger than the second argument\n";
        cmds += "=? -- checks if first arg is equal to second argument\n";

        cmds += "substr -- Returns a string that is a substring of this string. begins with the char at the first num and with last char -1.\n"
                + "Example:\n" + " \"unhappy\".substring(2) returns \"happy\"\n";

        cmds += "strlen returns the length of a string";

        return cmds;
    }

}
