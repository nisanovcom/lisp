package virtualMachine.primitives;

import java.io.IOException;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispProgram;
import lispDataTypes.LispString;
import lispDataTypes.lispNumbers.LispInt;
import lispInterpreter.LispCompiler;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVM;
import virtualMachine.LispVarTable;

public class LispImport extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        if (!(isParamsValid(lispCmd, 1, "import expects only 1 parameter!"))) {
            // TODO throw exception here
            int x = 0 / 0;
        }

        LispObject objEval = LispEvaluator.eval(lispCmd.getArg(1), env);
        if (!(objEval instanceof LispString)) {
            // TODO throw exception here
            System.out.println(
                    "Error: import did not recieve a string but a " + objEval.getClass().getSimpleName() + " instead");
            int x = 0 / 0;
        }

        String fileName = objEval.getStringVal();
        LispProgram importProgram = null;
        try {
            importProgram = LispCompiler.compile(fileName);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        LispVM importVM = new LispVM(importProgram, env);
        importVM.run();
        return new LispInt(1);
    }

}
