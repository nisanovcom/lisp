package virtualMachine.primitives;

import java.util.ArrayList;
import java.util.List;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispProgram;
import lispDataTypes.LispString;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;
import virtualMachine.lispProcedures.LispUserProcedure;

public class LispLambda extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        List<LispObject> procedureBody = new ArrayList<>();
        for (int i = 2; i < lispCmd.getNumArgs(); i++) {
            procedureBody.add(lispCmd.getArg(i));
        }
        
        List<LispString> argsNames = getArgsNames(lispCmd);
        LispProgram lispProcBody = new LispProgram(procedureBody);
//        System.out.println("Excecuting lambda!");
        LispObject newUserProc = new LispUserProcedure(lispProcBody, argsNames, env);
        return newUserProc;
    }

    private List<LispString> getArgsNames(LispCommand lispCmd) {
        if(!(lispCmd.getArg(1) instanceof LispCommand)) {
            // TODO throw exception here
            System.out.println("Error: lambda procedure needs to recieve a list of parameters as second argument");
            int x = 0/0;
        }
        LispCommand rawArgs = (LispCommand) lispCmd.getArg(1);
        List<LispString> args = new ArrayList<>();
        for(int i=0; i<rawArgs.getNumArgs(); i++) {
            LispObject curObj = rawArgs.getArg(i);
            if(!LispEvaluator.isVarName(curObj)) {
                // TODO throw exception here
                System.out.println("invalid argument name for a function!");
            }
            LispString curString = (LispString) curObj;
            args.add(curString);
        }
        return args;
    }

    // @Override
    // public String getStringVal() {
    // return "Lisp Primitive Lambda";
    // }
}
