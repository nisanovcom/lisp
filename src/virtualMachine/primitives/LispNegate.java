package virtualMachine.primitives;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.lispNumbers.LispInt;
import lispDataTypes.lispNumbers.LispNumber;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;

public class LispNegate extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        int neg = 0;
        LispObject arg1Eval = LispEvaluator.eval(lispCmd.getArg(1), env);
        if (!(arg1Eval instanceof LispNumber)) {
            // TODO throw exception here
            System.out.println("obj: " + arg1Eval.getStringVal() + " is not a number but a "
                    + lispCmd.getArg(1).getClass().getSimpleName());
            int x = 0 / 0;
        }
        if (lispCmd.getNumArgs() == 2) {
            neg = -(int) ((LispNumber) arg1Eval).getNumberValue();
        } else {
            neg = (int) ((LispNumber) arg1Eval).getNumberValue();
            for (int i = 2; i < lispCmd.getNumArgs(); i++) {
                LispObject lispObj = lispCmd.getArg(i);
                LispObject evaledObj = LispEvaluator.eval(lispObj, env);
                if (!(evaledObj instanceof LispNumber)) {
                    // TODO throw exception here
                    System.out.println("obj: " + lispObj.getStringVal() + " is not a number!");
                    int x = 0 / 0;
                }
                // TODO remove casting from here, so well be able to act on floats as well
                neg -= (int) ((LispNumber) evaledObj).getNumberValue();
            }
        }
        return new LispInt(neg);
    }

}
