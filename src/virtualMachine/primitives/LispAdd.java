package virtualMachine.primitives;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispString;
import lispDataTypes.lispNumbers.LispInt;
import lispDataTypes.lispNumbers.LispNumber;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;

public class LispAdd extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        boolean isNum = true;

        String combinedStr = "";
        int sum = 0;
        for (int i = 1; i < lispCmd.getNumArgs(); i++) {
            LispObject lispObj = lispCmd.getArg(i);
            LispObject evaledObj = LispEvaluator.eval(lispObj, env);
            if (!(evaledObj instanceof LispNumber)) {
                isNum = false;

                if (!(evaledObj instanceof LispString)) {
                    /*
                     * Note that the string check is in the "else if" section since if an object is
                     * a number it could be represented as a string as well
                     */

                    // TODO throw exception here
                    System.out.println("type: " + evaledObj.getClass().getSimpleName());
                    System.out.println("obj: " + lispObj.getStringVal() + " is not a number!");
                    int x = 0 / 0;

                }
            }
            if (isNum) {
                // TODO remove casting from here, so well be able to act on floats as well
                sum += (int) ((LispNumber) evaledObj).getNumberValue();
            }
            combinedStr += evaledObj.getStringVal();
        }

        if (isNum) {
            return new LispInt(sum);
        } else {
            return new LispString(combinedStr);
        }
    }

}
