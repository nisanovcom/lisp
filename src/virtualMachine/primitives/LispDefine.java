package virtualMachine.primitives;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispString;
import lispDataTypes.LispVarPair;
import lispDataTypes.lispNumbers.LispInt;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;

public class LispDefine extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        // Note: one arg is taken for the procedure itself...
        if(lispCmd.getNumArgs() != 3) {
            // TODO add exception here!
            System.out.println("define recieves exactly 2 args!");
            int x=0/0;
        }
        if(!LispEvaluator.isVarName(lispCmd.getArg(1))) {
            // TODO add exception here!
            System.out.println("cant assign string to variable!");
        }
        String varName = ((LispString) lispCmd.getArg(1)).getValue();
        LispObject evalExp = LispEvaluator.eval(lispCmd.getArg(2), env);
        LispVarPair lispVarPair = new LispVarPair(varName, evalExp);
        env.addVar(lispVarPair);
        return new LispInt(1);
    }
    
    public String getStringVal() {
        return "LispDefine Primitive";
    }


}
