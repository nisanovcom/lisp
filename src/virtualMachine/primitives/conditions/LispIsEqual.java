package virtualMachine.primitives.conditions;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispString;
import lispDataTypes.lispNumbers.LispInt;
import lispDataTypes.lispNumbers.LispNumber;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;
import virtualMachine.primitives.LispPrimitiveProcedure;

public class LispIsEqual extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        if (!isParamsValid(lispCmd, 2, "isEqual recieves 2 parameters only!")) {
            // TODO throw exception here
            return null;
        }

        LispObject obj1 = LispEvaluator.eval(lispCmd.getArg(1), env);
        LispObject obj2 = LispEvaluator.eval(lispCmd.getArg(2), env);
        
        
        // TODO: write a more good looking testing
        if ((obj1 instanceof LispNumber || obj1 instanceof LispString)
                && (obj2 instanceof LispNumber || obj2 instanceof LispString)) {
            if (obj1.getStringVal().equals(obj2.getStringVal())) {
                return new LispInt(1);
            } else {
                return new LispInt(0);
            }
        } else {
            if (!obj1.getClass().equals(obj2.getClass())) {
                return new LispInt(0);
            } else {
                if (obj1.getStringVal().equals(obj2.getStringVal())) {
                    return new LispInt(1);
                } else {
                    return new LispInt(0);
                }
            }
        }

    }

}
