package virtualMachine.primitives.conditions;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.lispNumbers.LispInt;
import lispDataTypes.lispNumbers.LispNumber;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;
import virtualMachine.primitives.LispPrimitiveProcedure;

public class LispIf extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        if(!isParamsValid(lispCmd, 3, "If procedure takes 3 parameters")) {
            // TODO throw exception here
            int x=0/0;
            return null;
        }
        LispObject conditionEval = LispEvaluator.eval(lispCmd.getArg(1), env);
        if(!(conditionEval instanceof LispInt)) {
            // TODO throw exception here
            System.out.println("if condition accepts only a integer!");
            int x=0/0;
        }
        
        Integer condVal = ((LispInt) conditionEval).getNumberValue();
        if(condVal > 0) {
            return LispEvaluator.eval(lispCmd.getArg(2), env);
        } else {
            return LispEvaluator.eval(lispCmd.getArg(3), env);
        }
        
    }

}
