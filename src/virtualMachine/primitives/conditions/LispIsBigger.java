package virtualMachine.primitives.conditions;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.lispNumbers.LispInt;
import lispDataTypes.lispNumbers.LispNumber;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;
import virtualMachine.primitives.LispPrimitiveProcedure;

public class LispIsBigger extends LispPrimitiveProcedure {

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        if(!isParamsValid(lispCmd, 2, "isBigger recieves 2 parameters only!")) {
            // TODO throw exception here
            return null;
        }
        
        LispObject obj1 = LispEvaluator.eval(lispCmd.getArg(1), env);
        LispObject obj2 = LispEvaluator.eval(lispCmd.getArg(2), env);
        if(!(obj1 instanceof LispNumber) || !(obj2 instanceof LispNumber)) {
            // TODO throw exception here
            System.out.println("isBigger accepts only numbers, recieved: " + obj1.getClass().getSimpleName() + " and " + obj2.getClass().getSimpleName());
            int x=0/0;
            return null;
        }
        
        LispNumber val1 = ((LispNumber) obj1);
        LispNumber val2 = ((LispNumber) obj2);
        if(val1.isBigger(val2) > 0) {
            return new LispInt(1);
        } else {
            return new LispInt(0);
        }
    }

}
