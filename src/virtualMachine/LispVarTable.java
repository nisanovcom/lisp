package virtualMachine;

import java.util.ArrayList;
import java.util.List;

import lispDataTypes.LispObject;
import lispDataTypes.LispVarPair;

public class LispVarTable {
    private List<LispVarPair> varTable = new ArrayList<>();
    private LispVarTable father;


    public LispVarTable() {
        father = null;
    }


    public LispVarTable(LispVarTable father) {
        this.father = father;
    }


    public void addVar(LispVarPair newVarPair) {
        String varName = newVarPair.getVarName();
        LispObject varValue = newVarPair.getValue();

        // System.out.println("adding var: " + varName + " of type: " +
        // varValue.getClass().getSimpleName());

        for (LispVarPair curVarPair : varTable) {
            String curVarName = curVarPair.getVarName();
            if (varName.equals(curVarName)) {
                // System.out.println("Var exists, rewriting it!");
                curVarPair.setValue(varValue);
                return;
            }
            // else
            // System.out.println("Var " + varName + " != " + curVarName);
        }

        // var does not exist in table, add new one!
        // System.out.println("Var " + varName + " doesn't exist! adding new one! to
        // val: " + varValue.getStringVal());
        varTable.add(newVarPair);
        return;
    }


    public void setVar(LispVarPair newVarPair) {

        String varName = newVarPair.getVarName();
        LispObject varValue = newVarPair.getValue();
        for (LispVarPair lispVarPair : varTable) {
            String curVarName = lispVarPair.getVarName();
            if (varName.equals(curVarName)) {
                lispVarPair.setValue(varValue);
                return;
            }
        }

        if (father == null) {
            // TODO throw exception here
            System.out.println("Exception! value \"" + varName + "\" is not defined!");
        }
        father.setVar(newVarPair);
    }


    public boolean isVarExists(String varName) {
        for (LispVarPair pair : varTable) {
            if (varName.equals(pair.getVarName())) {
                // System.out.println("var " + varName + " EXISTS!");
                return true;
            }
        }
        // System.out.println("var " + varName + " NOT EXISTS!");
        return false;
    }


    public LispObject getValue(String objName) {
        for (LispVarPair varPair : varTable) {
            // System.out.println("comparing " + objName + " to " + varPair.getVarName() + "
            // is "
            // + (objName == varPair.getVarName()));
            if (varPair.getVarName().equals(objName)) {
                LispObject varValue = varPair.getValue();
                // System.out.println("Found " + objName + "! type: " +
                // varValue.getClass().getSimpleName() + "Value: "+varValue.getStringVal());
                return varValue;
            }
        }

        if (father == null) {
            // TODO throw exception here
            System.out.println("error at LispVarTable: cant find variable named: " + objName);
        }
        return father.getValue(objName);
    }
}
