package virtualMachine;

import lispDataTypes.*;

public class LispVM {
    private LispProgram lispProgram;
    // private int pc = 0;
    LispVarTable varTable = new LispVarTable();


    public LispVM(LispProgram lispProgram, LispVarTable varTable) {
        this.lispProgram = lispProgram;
        this.varTable = varTable;
    }


    public LispVM(LispProgram lispProgram) {
        this.lispProgram = lispProgram;
    }


    public void loadProgram(LispProgram lispProgram) {
        this.lispProgram = lispProgram;
    }


    public String run() {
        return run(varTable);
    }


    public String run(LispVarTable varTable) {
        this.varTable = varTable;
        // System.out.println("Executing command:\n" + lispProgram.dumpProgram());
        // System.out.println("start running VM");
        // System.out.println("--------------------------------------");
        String programOutput = "";
        for (int i = 0; i < lispProgram.getProgramSize(); i++) {
            LispObject curCmd = lispProgram.getCmd(i);
            // System.out.println("EVAL!!!: " + curCmd.getStringVal());
            String cmdVal = LispEvaluator.eval(curCmd, varTable).getStringVal();
        }
        // System.out.println("--------------------------------------");
        // System.out.println("DONE RUNNING VM");
        return programOutput;
    }

}
