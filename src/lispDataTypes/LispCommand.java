package lispDataTypes;

import java.util.List;

public class LispCommand implements LispObject {
    private List<LispObject> args;


    public LispCommand(List<LispObject> cmdArgs) {
        args = cmdArgs;
    }


    public int getNumArgs() {
        return args.size();
    }


    public LispObject getArg(int argIdx) {
        return args.get(argIdx);
    }


    public String getStringVal() {
        String toRet = "[";
        // for(LispObject lo : args) {
        for (int i = 0; i < args.size() - 1; i++) {
            LispObject lo = args.get(i);
            toRet += lo.getStringVal();
            toRet += ", ";
        }
        if (args.size() <= 1) {
            return toRet + "]";
        } else {
//            System.out.println("Args: " + args.size());
            toRet += args.get(args.size() - 1).getStringVal();
            toRet += "]";
        }
        return toRet;
    }

}
