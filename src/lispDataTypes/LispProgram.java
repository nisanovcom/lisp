package lispDataTypes;

import java.util.List;

public class LispProgram {
    private List<LispObject> commands;


    public LispProgram(List<LispObject> cmds) {
        commands = cmds;
    }


    public List<LispObject> getCommands() {
        return commands;
    }


    public void setCommands(List<LispObject> commands) {
        this.commands = commands;
    }


    public int getProgramSize() {
        return commands.size();
    }

    
    public String dumpProgram() {
        String toRet = "";
        for(LispObject lc : commands) {
            toRet += "Type: " + lc.getClass().getSimpleName() + " val: " + lc.getStringVal();
            toRet += "\n";
        }
//        System.out.println(toRet);
        return toRet;
    }
    
    
    public LispObject getCmd(int index) {
        return commands.get(index);
    }
}
