package lispDataTypes;

public class LispVarPair {
    private String varName;
    private LispObject value;


    public LispVarPair(String name, LispObject value) {
        this.setVarName(name);
        this.setValue(value);
    }


    public String getVarName() {
        return varName;
    }


    public void setVarName(String name) {
        this.varName = name;
    }


    public LispObject getValue() {
        return value;
    }


    public void setValue(LispObject value) {
        this.value = value;
    }
}
