package lispDataTypes.lispNumbers;

import lispDataTypes.LispObject;

public abstract class LispNumber<T extends Number> implements LispObject {
    private T value;
    
    public T getNumberValue() {
        return this.value;
    }
    
    public int isBigger(LispNumber comparedNumber) {
        if(this instanceof LispInt && comparedNumber instanceof LispInt) {
            if(((int) this.getNumberValue()) > ((int) comparedNumber.getNumberValue())) {
                return 1;
            } else {
                return 0;
            }
        } else {
            // TODO add more handlers
            System.out.println("Interpreter is yet to know how to compare numbers which is not an integer!");
            return -1;
        }
    }
    
    public void setNumberValue(T newValue) {
        this.value = newValue;
    }
    
    public String getStringVal() {
        return value.toString();
    }
}
