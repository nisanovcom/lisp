package lispDataTypes.lispNumbers;

public class LispInt extends LispNumber<Integer> {

    public LispInt(int intVal) {
        setNumberValue(intVal);
    }

}
