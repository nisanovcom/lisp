package lispInterpreter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispProgram;
import lispDataTypes.LispString;
import lispDataTypes.lispNumbers.LispInt;

public class LispCompiler {

    public static LispProgram compile(String src) throws IOException {
//        System.out.println("src = " + src);
        FileInputStream fileStream = new FileInputStream(src);
        BufferedReader in = new BufferedReader(new InputStreamReader(fileStream, "UTF8"));
        String argsBuffer = "";
        char curChar;
        List<LispObject> lispCommands = new ArrayList<>();
        // boolean isMainExpr = true;
        int bracketCounter = 0;
        try {
            int charVal;
            
            while ((charVal = in.read()) != -1) {
                curChar = (char) charVal;
//                System.out.println("got char: " + curChar);
                if(curChar == '#') {
//                    System.out.println("got a comment!");
                    while((charVal = in.read()) != -1) {
                        curChar = (char) charVal;
//                        System.out.println("comment char: |" + curChar + "|");
                        if((curChar) == '\n') {
                            break;
                        }
                    }
                }
                if (curChar != '\n') {
                    argsBuffer += curChar;
                }

                if (curChar == '(') {
                    bracketCounter++;
                } else if (curChar == ')') {
                    bracketCounter--;
                    // if(bracketCounter == 0 && !isMainExpr) {
                    if (bracketCounter == 0) {
                        // isMainExpr = true;
                        LispObject curObject = compileExpression(argsBuffer);
                        if (!(curObject instanceof LispCommand)) {
                            // TODO throw exception here
                            System.out.println("Error at compile time: " + argsBuffer + " is not a LispCommand??");
                            return null;
                        } else {
                            lispCommands.add((LispCommand) curObject);
                        }
                        argsBuffer = "";
                    }
                }
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return new LispProgram(lispCommands);
    }


    private static LispObject compileExpression(String arg) {
//        System.out.println("Compiling expression: " + arg);
        if (isInt(arg)) {
            return makeInt(arg);
        } else if (isString(arg)) {
            return makeString(arg);
        } else if (isKeyWord(arg)) {
            return makeKeyWord(arg);
        } else if (isCommand(arg)) {
            return makeCommand(arg);
        }
        
        // TODO throw exception here
        System.out.println("Error at compile time: unknown expression: " + arg);
        int x = 0/0;
        return null;
    }


    private static LispCommand makeCommand(String arg) {
//        System.out.println("Making command: " + arg);
        List<LispObject> cmdArgs = new ArrayList<>();
        int start = -1;
        int bracketCounter = 0;
        for (int i = 1; i < arg.length() - 1; i++) {
            char curChar = arg.charAt(i);
//            System.out.println("char " + curChar + " is equal to '('?" + (curChar == '('));
            if (!isSpaceChar(curChar) && curChar != '(' && curChar != ')') {
                if (start == -1) {
                    start = i;
                }
            } else if (curChar == '(') {
                if(bracketCounter == 0) {
                    start = i;
                }
//                System.out.println("PLUS!");
                bracketCounter++;
            } else if (curChar == ')') {
//                System.out.println("MINUS!");
                bracketCounter--;
            } else if (bracketCounter == 0) {
                if (start != -1) {
                    String expression = arg.substring(start, i);
//                    System.out.println("Got subExpr: " + expression);
                    LispObject curObject = compileExpression(expression);
                    cmdArgs.add(curObject);
                    start = -1;
                }
            }
        }
        
        if(start != -1) {
            String lastExpr = arg.substring(start, arg.length()-1);
            LispObject lastObj = compileExpression(lastExpr);
            cmdArgs.add(lastObj);
        }

        return new LispCommand(cmdArgs);
    }


    private static boolean isSpaceChar(char charToCheck) {
        if (charToCheck == ' ' || charToCheck == '\t') {
            return true;
        }
        return false;
    }


    private static LispString makeKeyWord(String arg) {
        return new LispString(arg);
    }


    private static LispString makeString(String arg) {
//        String argStr = arg.substring(1, arg.length()-1);
//        System.out.println("Making string: " + argStr);
        return new LispString(arg);
    }


    private static LispInt makeInt(String arg) {
        return new LispInt(Integer.parseInt(arg));
    }


    private static boolean isCommand(String arg) {
        if (arg.charAt(0) == '(' && arg.charAt(arg.length() - 1) == ')') {
            return true;
        }
        return false;
    }


    private static boolean isKeyWord(String arg) {
        // TODO add support for not only letters and numbers
        if (!Character.isLetter(arg.charAt(0)) &&!isSpecialVarChar(arg.charAt(0))) {
            return false;
        }

        for (int i = 1; i < arg.length(); i++) {
            char curChar = arg.charAt(i);
            if ((!Character.isLetter(curChar) && !Character.isDigit(curChar)) && !isSpecialVarChar(curChar)) {
                return false;
            }
        }
        return true;
    }
    
    private static boolean isSpecialVarChar(char toCheckChar) {
        String toCompare = "-_?+*/!@$%^&<>=";
        for(int i=0; i<toCompare.length(); i++) {
            if(toCheckChar == toCompare.charAt(i)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isInt(String arg) {
        for (int i = 0; i < arg.length(); i++) {
            char character = arg.charAt(i);
            if (!Character.isDigit(character)) {
                return false;
            }
        }
        return true;
    }


    private static boolean isString(String arg) {
//        System.out.println("start: " + arg.charAt(0));
//        System.out.println("end: " + arg.charAt(arg.length() - 1));
        if (arg.charAt(0) == '\'' && arg.charAt(arg.length() - 1) == '\'') {
            return true;
        }
        return false;
    }

}
