#(print 'loading_library')
(define null (lambda () (print 'NULL')))

# NOTE: this is possible since the '-' method is defined as a primitive in its core
#       and we first look for users methods thus it works
#(define - (lambda (x) (- 0 x)))


(define pair (lambda (first sec)
                (lambda (action attr1 attr2)
                    (if (=? action 'get')
                        (if (=? attr1 'first')
                            first
                            sec)
                        (if (=? action 'set')
                            (if (=? attr1 'first')
                                (set first attr2)
                                (set sec attr2))
                            (print 'unknown_action:_' action)
                            )))))
#

(define new-list (lambda (first-val)
                         (pair first-val null)))


(define get-first (lambda (varPair) (varPair 'get' 'first' 0)))
(define set-first (lambda (varPair new-first) (varPair 'set' 'first' new-first)))

(define get-sec (lambda (varPair) (varPair 'get' 'sec' 0)))
(define set-sec (lambda (varPair new-sec) (varPair 'set' 'sec' new-sec)))


(define iterate (lambda (iter-times loop-proc)
                    (define inner-iterate (lambda (cur-time)
                                            (if (> cur-time iter-times)
                                                1
                                                ((lambda ()
                                                    (loop-proc cur-time)
                                                    (inner-iterate (+ cur-time 1)))))))
                                          #
                    #
                    (inner-iterate 1)))
#

(define * (lambda (num1 num2)
            (define counter 0)
            (iterate num2 (lambda (i) (set counter (+ num1 counter))))
            counter))

(define get-in-list (lambda (list item-pos)
                        (define get-in-list-internal (lambda (intern-list count)
                            (if (=? intern-list null)
                                null
                                (if (=? item-pos count)
                                    (get-first intern-list)
                                    (get-in-list-internal (get-sec intern-list) (+ 1 count))))))
                        (get-in-list-internal list 0)
                    ))

(define list-len (lambda (list)
                    (define list-len-intern (lambda (intern-list count)
                                                    (if (=? intern-list null)
                                                        count
                                                        (list-len-intern (get-sec intern-list) (+ 1 count)))))
                    (list-len-intern list 0)))
#


(define get-last (lambda (list)
                            (get-in-list list (- (list-len list) 1))))
#


(define get-last-node (lambda (list)
                                (if (=? (get-sec list) null)
                                    list
                                    (get-last-node (get-sec list)))))
#

(define append (lambda (list newVal)
                       (define last-node (get-last-node list))
                       (set-sec last-node (pair newVal null))))

(define filter (lambda (list check)
                (if (=? list null)
                    null
                    (if (check (get-first list))
                        (pair (get-first list) (filter (get-sec list) check))
                        (filter (get-sec list) check)))
                ))
#


# returns how many times item appears in list, 0 if not appears at all
(define is-in-list (lambda (list obj)
                           (define identical-objs (filter list (lambda (item)
                                                                       (=? item obj))))
                           (list-len identical-objs)))


(define map (lambda (list func)
                    (if (=? list null)
                        null
                        (pair (func (get-first list)) (map (get-sec list) func)))))

(define print-list (lambda (list)
                    (map list (lambda (x) (print x)))
                    1))

(define make-basicDict (lambda (first-key first-val)
                        (define dict-list (pair (pair first-key first-val) null))
                        (define get-key (lambda (key)
                            (define key-pair (filter dict-list (lambda (x) (=? (get-first x) key))))
                            (if (=? key-pair null)
                                null
                                (get-sec (get-first key-pair)))))
                        (define put-key (lambda (key val)
                                            (define key-pair (filter dict-list
                                                        (lambda (x) (=? (get-first x) key))))
                                            (if (=? key-pair null)
                                                (set-sec (get-last-node dict-list) (pair (pair key val) null))
                                                ((lambda ()
                                                        (set-sec (get-first key-pair) val))))))
                        (define get-all-keys (lambda ()
                                                     (map dict-list 
                                                          (lambda (item)
                                                                  (get-first item)))))
                        
                        (lambda (action attr1 attr2)
                                (if (=? action 'get')
                                    (get-key attr1)
                                    (if (=? action 'put')
                                        (put-key attr1 attr2)
                                        (if (=? action 'get-all-keys')
                                            (get-all-keys)
                                            (print 'warn:_unknown_action:_' action))))
                                        )))
#

(define put-key      (lambda (dict key val)
                             (dict 'put' key val)))
(define get-key      (lambda (dict key)
                             (dict 'get' key null)))
(define get-all-keys (lambda (dict)
                             (dict 'get-all-keys' null null)))
#


(define copy-basicDict (lambda (dict)
                       (define dict-keys (get-all-keys dict))
                       (define first-key (get-first dict-keys))
                       (define basicDict-copy (make-basicDict first-key (get-key dict first-key)))
                       (map dict-keys (lambda (key) (put-key basicDict-copy key (get-key dict key))))
                       basicDict-copy))

(define create-basicClass (lambda (class-name)
                             (define properties (make-basicDict '__type__' class-name))
                             (define add-property (lambda (property-name property-val) 
                                                          (if (is-in-list (get-all-keys properties) property-name)
                                                              (print (+ 'WARN_::_already_assigned_property:_' new-property))
                                                              (put-key properties property-name property-val))))
                             (define add-var  (lambda (var-name)       (add-property var-name  null)))
                             (define add-proc (lambda (proc-name proc) (add-property proc-name proc)))

                             (define action-dispatch-table (make-basicDict 'add-var' 
                                      (lambda (var-name trash1)
                                              (add-var var-name))))
                             (put-key action-dispatch-table 'add-proc' 
                                      (lambda (proc-name proc)
                                              (add-proc proc-name proc)))
                             (put-key action-dispatch-table 'make-object' 
                                      (lambda (trash1 trash2) 
                                              (define private-properties (copy-basicDict properties))
                                              (lambda (obj-var)
                                                      (get-key private-properties obj-var))))
                             
                             (lambda (action attr1 attr2)
                                     ((get-key action-dispatch-table action) attr1 attr2))))
#


#(print 'done_loading_library')
#(print '')
