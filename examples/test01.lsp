
(define myXX 3)
(import 'library.lsp')

(define count-factory (lambda (start-num jump)
                        (lambda ()
                            (define to-return start-num)
                            (set start-num (+ start-num jump))
                                to-return)))
#


(define count1 (count-factory 2 1))
(define count2 (count-factory 0 5))

#(iterate 5 (lambda (i) (print i)))
#(print (* 3 7))
#(print (- 5))
#(print (=? 1 '1') 'dsa')
#(print (strlen 'hello'))


(define d (make-basicDict 'a' 1))
(print '-----')
(put-key d 'b' 2)
#(print 'key:_b' (get-key d 'b'))

(put-key d 'b' 10)
(put-key d 'a' 15)
(put-key d 'c' 'hello')
(put-key d 2 '2_here')
(put-key d '2' '2_herexxx')


#(print 'key:_b' (get-key d 'b'))
#(print 'key:_a' (get-key d 'a'))

(define d-copy (copy-basicDict d))
(define all-keys (get-all-keys d-copy))
(print 'ssss')
#(print-list all-keys)
(map all-keys (lambda (key)
                      (print (+ 'at_key:_' key '_val_is:_' (get-key d-copy key)))))
#
(define myL (new-list 1))
(append myL 2)
(append myL 2)
(append myL 3)
(append myL 'ssa')
(append myL 'asds')
(append myL 2)
(append myL 2)

#(print-list myL)
(print (is-in-list myL '3'))

(print '-----------')

